#!/bin/bash

# Filename: <run.sh>
# Copyright (C) <2022> Authors: <Antoine Leroux>
# 
# This program is free software: you can redistribute it and / or 
# modify it under the terms of the GNU General Public License as published 
# by the Free Software Foundation, either version 2 of the License. 
# 
# This program is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of  
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
# GNU General Public License for more details.  
# 
# You should have received a copy of the GNU General Public License  
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Get current path (normally root of repo)
ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
if [ -z "$1" ]
    then 
        HOME_FOLDER=/home/$USER/git/
    else
        # HOME_FOLDER=/home/$USER/git/
        HOME_FOLDER=$(echo $1 | xargs)
        echo "You changed default mounted path to $HOME_FOLDER"
fi

PLATFORM="$(uname -m)"
BASE_NAME="$USER-nvidia-triton-ai-srv-$PLATFORM"
CONTAINER_NAME="$BASE_NAME-container"

function print_color {
    tput setaf $1
    echo "$2"
    tput sgr0
}

function print_error {
    print_color 1 "$1"
}

function print_warning {
    print_color 3 "$1"
}

function print_info {
    print_color 2 "$1"
}



# Get the entry in the dpkg status file corresponding to the provied package name
# Prepend two newlines so it can be safely added to the end of any existing
# dpkg/status file.
function get_dpkg_status() {
    echo -e "\n"
    awk '/Package: '"$1"'/,/^$/' /var/lib/dpkg/status
}

ON_EXIT=()
function cleanup {
    for command in "${ON_EXIT[@]}"
    do
        $command
    done
}
trap cleanup EXIT

pushd .
cd $ROOT
ON_EXIT+=("popd")

# Prevent running as root.
if [[ $(id -u) -eq 0 ]]; then
    print_error "This script cannot be executed with root privileges."
    print_error "Please re-run without sudo and follow instructions to configure docker for non-root user if needed."
    exit 1
fi

# Check if user can run docker without root.
RE="\<docker\>"
if [[ ! $(groups $USER) =~ $RE ]]; then
    print_error "User |$USER| is not a member of the 'docker' group and cannot run docker commands without sudo."
    print_error "Run 'sudo usermod -aG docker \$USER && newgrp docker' to add user to 'docker' group, then re-run this script."
    print_error "See: https://docs.docker.com/engine/install/linux-postinstall/"
    exit 1
fi

# Check if able to run docker commands.
if [[ -z "$(docker ps)" ]] ;  then
    print_error "Unable to run docker commands. If you have recently added |$USER| to 'docker' group, you may need to log out and log back in for it to take effect."
    print_error "Otherwise, please check your Docker installation."
    exit 1
fi  

# Remove any exited containers.
if [ "$(docker ps -a --quiet --filter status=exited --filter name=$CONTAINER_NAME)" ]; then
    docker rm $CONTAINER_NAME > /dev/null
fi

# Re-use existing container.
if [ "$(docker ps -a --quiet --filter status=running --filter name=$CONTAINER_NAME)" ]; then
    print_info "Attaching to running container: $CONTAINER_NAME"
    docker exec -i -t -u $USER --workdir $HOME_FOLDER $CONTAINER_NAME /bin/bash 
    exit 0
fi

    
# Map host's display socket to docker
DOCKER_ARGS+=("-v /tmp/.X11-unix:/tmp/.X11-unix")
DOCKER_ARGS+=("-v $HOME/.Xauthority:/root/.Xauthority")
DOCKER_ARGS+=("-e DISPLAY=$DISPLAY")
DOCKER_ARGS+=("-e NVIDIA_VISIBLE_DEVICES=all")
DOCKER_ARGS+=("-e NVIDIA_DRIVER_CAPABILITIES=all")

print_info "Running $CONTAINER_NAME"
docker run -itd \
    --privileged --network host \
    ${DOCKER_ARGS[@]} \
    -v "/dev:/dev" \
    -v "/dev/svs_vistek:/dev/svs_vistek" \
    --mount type=bind,source=$HOME_FOLDER,target=$HOME_FOLDER \
    --mount type=bind,source="$HOME/.gitconfig",target="$HOME/.gitconfig" \
    --mount type=bind,source="$HOME/.ssh",target="$HOME/.ssh" \
    --name "$CONTAINER_NAME" \
    --user="$USER" \
    --gpus all \
    --entrypoint /usr/local/bin/scripts/workspace-entrypoint.sh \
    --workdir $HOME_FOLDER \
    $BASE_NAME \
    /bin/bash
